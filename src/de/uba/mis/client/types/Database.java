package de.uba.mis.client.types;

/**
 * Wrapper enum for the MESAP databases covered
 * 
 * @author Kevin Hausmann
 */
public enum Database {
	ZSE("ZSE_aktuell"),
	ZSE2020("ZSE_2020"),
	ZSE2021("ZSE_2021"),
	ZSE2022("ZSE_2022"),
	ZSE2023("ZSE_2023"),
	ZSE2024("ZSE_2024"),
	BEU("BEU"),
	POSO("PoSo_aktuell"),
	ESz("ESz"),
	Enerdat("Enerdat"),
	EMMa2022("EMMa_2022"),
	EMMa2023("EMMa_2023");
	
	private String id;
	
	private Database(String id) {
		this.id = id;
	}
	
	/**
	 * Get database as referred to in JSON
	 * @param id JSON identifier
	 * @return Database in question
	 */
	public static Database getByID(String id) {
		for (Database database : Database.values())
			if (database.id.equalsIgnoreCase(id)) return database;
		
		return null;
	}
	
	/**
	 * @return A nice short display name for the database
	 */
	public String getDisplayName() {
		switch (this) {
			case ZSE: return "ZSE";
			case ZSE2020: return "ZSE 2020";
			case ZSE2021: return "ZSE 2021";
			case ZSE2022: return "ZSE 2022";
			case ZSE2023: return "ZSE 2023";
			case ZSE2024: return "ZSE 2024";
			case BEU: return "BEU";
			case POSO: return "PoSo";
			case ESz: return "ESz";
			case Enerdat: return "Enerdat";
			case EMMa2022: return "EMMa 2022";
			case EMMa2023: return "EMMa 2023";
		}
		
		return null;
	}
	
	/**
	 * @return A nice full display name for the database
	 */
	public String getLongDisplayName() {
		switch (this) {
			case ZSE: return "ZSE aktuell";
			case ZSE2020: return "ZSE Submission 2020";
			case ZSE2021: return "ZSE Submission 2021";
			case ZSE2022: return "ZSE Submission 2022";
			case ZSE2023: return "ZSE Submission 2023";
			case ZSE2024: return "ZSE Submission 2024";
			case BEU: return "Bilanz der Emissionsursachen (BEU)";
			case POSO: return "Punktquellen (PoSo)";
			case ESz: return "Emissionsszenarien";
			case Enerdat: return "Enerdat";
			case EMMa2022: return "Emissionsminderungsmassnahmen";
			case EMMa2023: return "Emissionsminderungsmassnahmen 2023";
		}
		
		return null;
	}
}
